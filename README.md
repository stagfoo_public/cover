<p align="center"><img width="300px" src="dist/card.png" />
</p>
<p align="center">an experiment for creating pngs that would play music </p>
<hr>
<p align="center">
<a href="https://opensource.org/licenses/MIT">
  <img src="https://img.shields.io/badge/License-MIT-yellow.svg" />
</a>
</p>

---

# What did I learn
- I learned about power stegonography
- I learned cryptography can be fun
- I explored a UX for plying music closer to in person sharing.
- create a system similar to PICO8 save files

# Whats inside

- 🖼️ UI = [nanohtml](https://github.com/choojs/nanohtml)
- 🍖 Store = [Obake.js](https://github.com/stagfoo/obake)
- 🦴 Router = [Page.js](https://visionmedia.github.io/page.js/)
- 🍹 Styles = Just Javascript and strings [Joro](https://github.com/stagfoo/joro).


